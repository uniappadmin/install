

# UniappAdmin 开源可视化搭建后台安装程序

>访问 | [官网](http://www.uniappadmin.cn)



## 安装说明
1、安装程序基于go 语言开发，实现跨平台，跨操作系统安装,安装程序需要安装docker环境
如果您未提前安装docker环境，也可按照界面提示安装docker程序后继续完成安装,安装过程
中如遇到问题,可在社区寻求帮助。

2、docker 尽量使用较新版本,1.2 以下版本会出现兼容问题

3、低层采用的react框架，第一次打开可能会需要几十秒。

## 特点

- 💻 **支持Windows10**  
- 💻 **支持部分Windows7**  Windows 7  安装过程中可能发现无法使用docker, 此时您可尝试手动使用docker安装
- 🌴 **支持Liunx**
- 🚀 **支持Mac**




## 🍳 UniappAdmin 安装演示图


<div>
  <img src="https://file.9pigfly.com/install1.png" width="49%"/> <br /><br />
    <img src="https://file.9pigfly.com/install3.png" width="49%"/><br /><br />
  <img src="https://file.9pigfly.com/install2.png" width="49%"/>
</div>

## 一键安装


## 🔨 windows10  7

```windows10
./start_for_windows.exe   或者双击打开
```
## 🔨 Mac
```Mac
chmod 755  start_for_mac_darwin
./start_for_mac_darwin 
```

## 🔨 Liunx
```Liunx
chmod 755  start_for_linux
./start_for_linux   
```

## 📦 訪問端口

```Liunx
http://ip:5201
```
## 📦 以下是使用一键安装部署出现问题后， 需要手动安装的步骤

1、登录docker  密码：rj123456
```bash
docker login --username=renjie59420 registry.cn-shenzhen.aliyuncs.com
```

2、下载 docker 镜像
```bash
docker pull   registry.cn-shenzhen.aliyuncs.com/uniappadmin/uniappadmin
```

3、启动应用
```bash
docker run -it -d    -p 5212:5212     -p 5204:5204        -p 5207:5207  registry.cn-shenzhen.aliyuncs.com/uniappadmin/uniappadmin
```
4、进入容器
```bash
docker exec -it  容器id
```

5、启动应用
```bash
bash /mnt/start.sh 
```


6、手动安装镜像错误说明：
镜像默认是windows 下环境安装，如果您使用的是liunx 手动安装, 遇见react npm 报错时，需要进入到 /mnt/uniappadmin/admin目录,手动执行
```bash
cd /mnt/uniappadmin/admin
npm install 
npm run dev
npm start 
```

```bash
cd /mnt/uniappadmin/adminEditor
npm install 
npm run dev
npm start 
```
