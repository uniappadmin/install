module install

go 1.14

require (
	github.com/Joker/jade v1.0.0 // indirect
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/Shopify/goreferrer v0.0.0-20181106222321-ec9c9a553398 // indirect
	github.com/ThomasRooney/gexpect v0.0.0-20161231170123-5482f0350944
	github.com/Unknwon/goconfig v0.0.0-20191126170842-860a72fb44fd // indirect
	github.com/astaxie/beego v1.12.2
	github.com/aymerick/raymond v2.0.2+incompatible // indirect
	github.com/beego/i18n v0.0.0-20161101132742-e9308947f407
	github.com/beego/samples v0.0.0-20180420090448-1c696ee4905b
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0
	github.com/docker/go-units v0.4.0 // indirect
	github.com/eknkc/amber v0.0.0-20171010120322-cdade1c07385 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/flosch/pongo2 v0.0.0-20200529170236-5abacdfa4915 // indirect
	github.com/gorilla/schema v1.1.0 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/iris-contrib/blackfriday v2.0.0+incompatible // indirect
	github.com/iris-contrib/formBinder v5.0.0+incompatible // indirect
	github.com/iris-contrib/go.uuid v2.0.0+incompatible // indirect
	github.com/kataras/golog v0.0.18 // indirect
	github.com/kataras/iris v11.1.1+incompatible // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/klauspost/compress v1.10.10 // indirect
	github.com/microcosm-cc/bluemonday v1.0.3 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/ryanuber/columnize v2.1.0+incompatible // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
)

replace github.com/docker/docker v1.13.1 => github.com/docker/engine v0.0.0-20190219214528-cbe11bdc6da8
